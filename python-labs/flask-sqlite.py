from flask import Flask, request, jsonify
import sqlite3

app = Flask(__name__)


def init_db():
    # Create a SQLite database connection
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    # Create a cursos table
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS cursos (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre TEXT NOT NULL,
            categoria TEXT
        )
    ''')
    conn.commit()
    conn.close()

def getCursos():
    # Create a SQLite database connection
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM cursos')
    cursos = cursor.fetchall()
    cursos_list = [{'id': curso[0], 'nombre': curso[1], 'categoria': curso[2]} for curso in cursos]
    conn.close()
    return cursos_list

def addCurso(data):
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    nombre = data['nombre']
    categoria = data.get('categoria', '')
    cursor.execute('INSERT INTO cursos (nombre, categoria) VALUES (?, ?)', (nombre, categoria))
    conn.commit()
    conn.close()

def getCurso(id):
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM cursos WHERE id = ?', (id,))
    curso = cursor.fetchone()
    conn.close()
    return curso

def updateCurso(data,id):
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    nombre = data['nombre']
    categoria = data.get('categoria', '')
    cursor.execute('UPDATE cursos SET nombre=?, categoria=? WHERE id=?', (nombre, categoria, id))
    conn.commit()
    conn.close()

def deleteCurso(id):
    conn = sqlite3.connect('cognos.db')
    cursor = conn.cursor()
    cursor.execute('DELETE FROM cursos WHERE id = ?', (id,))
    conn.commit()
    conn.close()


# Create a new cursos
@app.route('/api/v1/cursos', methods=['POST'])
def create_curso():
    data = request.get_json()
    addCurso(data)
    return jsonify({'message': ' created successfully'}), 201

# Get all cursos
@app.route('/api/v1/cursos', methods=['GET'])
def get_cursos():
    cursos_list = getCursos()
    return jsonify({'cursos': cursos_list})
    

# Get a specific curso
@app.route('/api/v1/cursos/<int:curso_id>', methods=['GET'])
def get_curso(curso_id):
    curso = getCurso(curso_id)
    if curso:
        curso_dict = {'id': curso[0], 'nombre': curso[1], 'categoria': curso[2]}
        return jsonify(curso_dict)
    else:
        return jsonify({'message': 'Curso not found'}), 404

# Update a curso
@app.route('/api/v1/cursos/<int:curso_id>', methods=['PUT'])
def update_curso(curso_id):
    data = request.get_json()
    updateCurso(data,curso_id)
    return jsonify({'message': 'Curso updated successfully'})

# Delete a curso
@app.route('/api/v1/cursos/<int:curso_id>', methods=['DELETE'])
def delete_curso(curso_id):
    deleteCurso(curso_id)
    return jsonify({'message': 'Curso deleted successfully'})

if __name__ == '__main__':
    app.run(debug=True)

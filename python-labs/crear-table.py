import sqlite3

conn = sqlite3.connect('alumnos.db')
c = conn.cursor()

# crear tabla
c.execute(''' CREATE TABLE alumnos (
id integer primary key autoincrement,
nombres text,
apellidos text) ''')

# insertar registros a la tabla
c.execute(''' INSERT INTO alumnos
(nombres,apellidos)
VALUES ('Ana','Gomez') ''')

conn.commit()
conn.close()
import PySimpleGUI as sg
import requests

def post_data_to_api(api_url, data):
    try:
        headers = {'Content-Type': 'application/json'}
        response = requests.post(api_url, json=data, headers=headers)
        response.raise_for_status()  # Check if the request was successful
        result = response.json()
        return result
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")
        return None

url_api = 'http://10.100.240.22:3000/api/v1/personas'

layout = [
    [ sg.Combo(['GET','POST','PUT','DELETE'],key='-HTTP-') , sg.InputText(key='-URL-',default_text=url_api,font=('Verdana',10)) ],
    [sg.Button('Invoke API',font=('Verdana',10), key='invoke_btn')],
    [sg.Multiline(key='-BODY-',font=('verdana',10) ,size=(60, 20))],
]
window = sg.Window('REST API Invocation', layout)

while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED:
        break
    if event == 'invoke_btn':
        url = values['-URL-']
        http = values['-HTTP-']

        response = requests.get(url)
        window['-BODY-'].update(response.text)
window.close()
import sqlite3
conn = sqlite3.connect('alumnos.db')
c = conn.cursor()
c.execute('SELECT * FROM ALUMNOS')
# una forma de recuperar registros
#for row in c:
#    print(row)
# otra forma de recuperar registros
rows = c.fetchall()

if len(rows) > 0:
    for row in rows:
        print(row)
else:
    print("No existe registros")

conn.commit()
conn.close()